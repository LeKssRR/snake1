// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKE1_PlaerPawnBase_generated_h
#error "PlaerPawnBase.generated.h already included, missing '#pragma once' in PlaerPawnBase.h"
#endif
#define SNAKE1_PlaerPawnBase_generated_h

#define snake1_Source_snake1_PlaerPawnBase_h_12_SPARSE_DATA
#define snake1_Source_snake1_PlaerPawnBase_h_12_RPC_WRAPPERS
#define snake1_Source_snake1_PlaerPawnBase_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define snake1_Source_snake1_PlaerPawnBase_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlaerPawnBase(); \
	friend struct Z_Construct_UClass_APlaerPawnBase_Statics; \
public: \
	DECLARE_CLASS(APlaerPawnBase, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/snake1"), NO_API) \
	DECLARE_SERIALIZER(APlaerPawnBase)


#define snake1_Source_snake1_PlaerPawnBase_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPlaerPawnBase(); \
	friend struct Z_Construct_UClass_APlaerPawnBase_Statics; \
public: \
	DECLARE_CLASS(APlaerPawnBase, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/snake1"), NO_API) \
	DECLARE_SERIALIZER(APlaerPawnBase)


#define snake1_Source_snake1_PlaerPawnBase_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlaerPawnBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlaerPawnBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlaerPawnBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlaerPawnBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlaerPawnBase(APlaerPawnBase&&); \
	NO_API APlaerPawnBase(const APlaerPawnBase&); \
public:


#define snake1_Source_snake1_PlaerPawnBase_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlaerPawnBase(APlaerPawnBase&&); \
	NO_API APlaerPawnBase(const APlaerPawnBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlaerPawnBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlaerPawnBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlaerPawnBase)


#define snake1_Source_snake1_PlaerPawnBase_h_12_PRIVATE_PROPERTY_OFFSET
#define snake1_Source_snake1_PlaerPawnBase_h_9_PROLOG
#define snake1_Source_snake1_PlaerPawnBase_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	snake1_Source_snake1_PlaerPawnBase_h_12_PRIVATE_PROPERTY_OFFSET \
	snake1_Source_snake1_PlaerPawnBase_h_12_SPARSE_DATA \
	snake1_Source_snake1_PlaerPawnBase_h_12_RPC_WRAPPERS \
	snake1_Source_snake1_PlaerPawnBase_h_12_INCLASS \
	snake1_Source_snake1_PlaerPawnBase_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define snake1_Source_snake1_PlaerPawnBase_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	snake1_Source_snake1_PlaerPawnBase_h_12_PRIVATE_PROPERTY_OFFSET \
	snake1_Source_snake1_PlaerPawnBase_h_12_SPARSE_DATA \
	snake1_Source_snake1_PlaerPawnBase_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	snake1_Source_snake1_PlaerPawnBase_h_12_INCLASS_NO_PURE_DECLS \
	snake1_Source_snake1_PlaerPawnBase_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKE1_API UClass* StaticClass<class APlaerPawnBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID snake1_Source_snake1_PlaerPawnBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
