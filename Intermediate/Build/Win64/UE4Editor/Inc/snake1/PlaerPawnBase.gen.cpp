// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "snake1/PlaerPawnBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePlaerPawnBase() {}
// Cross Module References
	SNAKE1_API UClass* Z_Construct_UClass_APlaerPawnBase_NoRegister();
	SNAKE1_API UClass* Z_Construct_UClass_APlaerPawnBase();
	ENGINE_API UClass* Z_Construct_UClass_APawn();
	UPackage* Z_Construct_UPackage__Script_snake1();
// End Cross Module References
	void APlaerPawnBase::StaticRegisterNativesAPlaerPawnBase()
	{
	}
	UClass* Z_Construct_UClass_APlaerPawnBase_NoRegister()
	{
		return APlaerPawnBase::StaticClass();
	}
	struct Z_Construct_UClass_APlaerPawnBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APlaerPawnBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APawn,
		(UObject* (*)())Z_Construct_UPackage__Script_snake1,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlaerPawnBase_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "PlaerPawnBase.h" },
		{ "ModuleRelativePath", "PlaerPawnBase.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APlaerPawnBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APlaerPawnBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APlaerPawnBase_Statics::ClassParams = {
		&APlaerPawnBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_APlaerPawnBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APlaerPawnBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APlaerPawnBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APlaerPawnBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APlaerPawnBase, 69660752);
	template<> SNAKE1_API UClass* StaticClass<APlaerPawnBase>()
	{
		return APlaerPawnBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APlaerPawnBase(Z_Construct_UClass_APlaerPawnBase, &APlaerPawnBase::StaticClass, TEXT("/Script/snake1"), TEXT("APlaerPawnBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APlaerPawnBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
