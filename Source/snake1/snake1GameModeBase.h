// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "snake1GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE1_API Asnake1GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
