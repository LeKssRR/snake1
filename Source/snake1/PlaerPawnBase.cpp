// Fill out your copyright notice in the Description page of Project Settings.


#include "PlaerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
// Sets default values
APlaerPawnBase::APlaerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera; 
}

// Called when the game starts or when spawned
void APlaerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlaerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlaerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

